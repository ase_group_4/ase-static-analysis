package ch.zhaw;

import ch.zhaw.catan.Config;
import ch.zhaw.catan.Config.Faction;
import ch.zhaw.catan.Config.Resource;
import ch.zhaw.catan.SiedlerGame;
import ch.zhaw.catan.model.*;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Model class for {@code Settlement}
 * This class stores the faction which owns the {@code Settlement}
 *
 * @author Alina, Ridvan, Nils, Pascal
 * @version 04.12.2020
 */
public class Gruppe14SiedlerGameTest {

    /**
     * Tests if initial road can be build on Water.
     */
    @Test
    public void testBuildInitialRoadCannotBeBuildOnWater() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Point startRoad = new Point(12, 10);
        Point endRoad = new Point(13, 9);
        assertTrue(siedlerGame.placeInitialSettlement(new Point(12, 10), false));
        assertFalse(siedlerGame.placeInitialRoad(startRoad, endRoad));
    }

    /**
     * Test if initial road cannot be built because No Settlement is there
     */
    @Test
    public void testPlaceInitialRoadCannotBeBuildBecauseNoSettlement() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Point startRoad = new Point(10, 16);
        Point endRoad = new Point(9, 15);

        assertFalse(siedlerGame.placeInitialRoad(startRoad, endRoad));
    }

    /**
     * Test if initial road can be built
     */
    @Test
    public void testPlaceInitialRoad() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Point settlementPoint = new Point(7, 7);

        assertTrue(siedlerGame.placeInitialSettlement(settlementPoint, false));
        Point startRoad = new Point(7, 7);
        Point endRoad = new Point(6, 6);

        assertTrue(siedlerGame.placeInitialRoad(startRoad, endRoad));
    }

    /**
     * Test if Road cannot be build because the player doesn't have enough resources
     */
    @Test
    public void testRoadCannotBuildBecauseNoResources() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Point pointStart = new Point(5, 13);
        Point pointEnd = new Point(6, 12);

        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 13), false));

        assertFalse(siedlerGame.buildRoad(pointStart, pointEnd));
    }

    /**
     * Test if road can't be built because it would only be connected to an opponents road
     */
    @Test
    public void testBuildRoadOnlyConnectedToOpponentRoad() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 2);
        //initial setup
        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 9), false));
        assertTrue(siedlerGame.placeInitialRoad(new Point(5, 9), new Point(5, 7)));
        siedlerGame.switchToNextPlayer();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(3, 9), false));
        assertTrue(siedlerGame.placeInitialRoad(new Point(3, 7), new Point(3, 9)));
        siedlerGame.switchToPreviousPlayer();

        assertFalse(siedlerGame.buildRoad(new Point(3, 9), new Point(2, 10)));
    }

    /**
     * Test if Road can be build and the players resource stock is decreased
     */
    @Test
    public void testBuildRoadAndCheckPlayerResourcesAfterBuild() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();
        bank.payoutResource(player, Resource.CLAY, 1);
        bank.payoutResource(player, Resource.WOOD, 1);
        player.getResourcesAsList();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 7), false));

        Point pointStart = new Point(5, 7);
        Point pointEnd = new Point(6, 6);
        assertTrue(siedlerGame.buildRoad(pointStart, pointEnd));
        assertEquals(0, siedlerGame.getCurrentPlayerResourceStock(Resource.CLAY));
        assertEquals(0, siedlerGame.getCurrentPlayerResourceStock(Resource.WOOD));
    }

    /**
     * Test if the Road can't be built because the edge is already occupied
     */
    @Test
    public void testCannotBuildRoadBecausePlaceOccupied() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();
        bank.payoutResource(player, Resource.CLAY, 2);
        bank.payoutResource(player, Resource.WOOD, 2);
        player.getResourcesAsList();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 7), false));

        Point pointStart = new Point(5, 7);
        Point pointEnd = new Point(6, 6);
        assertTrue(siedlerGame.buildRoad(pointStart, pointEnd));
        assertFalse(siedlerGame.buildRoad(pointStart, pointEnd));
    }

    /**
     * tests if its possible to build more roads then the defined stockPerPlayer in the config
     */
    @Test
    public void testBuildRoadStructureNotAvailableForPlayer() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);

        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 9), false));
        for (int i = 0; i < Config.Structure.ROAD.getStockPerPlayer(); i++) {
            siedlerGame.getCurrentPlayer().getRoads().add(new Road(siedlerGame.getCurrentPlayerFaction(), null, null));
        }
        assertFalse(siedlerGame.buildRoad(new Point(5, 9), new Point(5, 7)));
    }

    /**
     * tests if its possible to possible to build more settlements then the defined stockPerPlayer in the config
     */
    @Test
    public void testBuildSettlementStructureNotAvailableForPlayer() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);

        for (int i = 0; i < Config.Structure.SETTLEMENT.getStockPerPlayer(); i++) {
            siedlerGame.getCurrentPlayer().getSettlements().add(new Settlement(siedlerGame.getCurrentPlayerFaction()));
        }
        assertFalse(siedlerGame.placeInitialSettlement(new Point(5, 9), false));
    }

    /**
     * tests if its possible to build more cities then the defined stockPerPlayer in the config
     */
    @Test
    public void testBuildCityStructureNotAvailableForPlayer() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);

        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 9), false));
        for (int i = 0; i < Config.Structure.CITY.getStockPerPlayer(); i++) {
            siedlerGame.getCurrentPlayer().getSettlements().add(new City(siedlerGame.getCurrentPlayerFaction()));
        }
        assertFalse(siedlerGame.buildCity(new Point(5, 9)));
    }

    /**
     * tests if its possible to build outside of the map
     */
    @Test
    public void testBuildLocationOutsideMap() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);

        //negative coordinates
        assertFalse(siedlerGame.placeInitialSettlement(new Point(-5, -5), false));
        //positive coordinates outside of the map
        assertFalse(siedlerGame.placeInitialSettlement(new Point(35, 35), false));
        //x coordinates outside of the map
        assertFalse(siedlerGame.placeInitialSettlement(new Point(27, 5), false));
        //y coordinates outside of the map
        assertFalse(siedlerGame.placeInitialSettlement(new Point(5, 44), false));
    }

    /**
     * tests if its possible to build a road with two coordinates who are not connected to each other
     */
    @Test
    public void testBuildRoadInvalidEdgeCoordinate() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);

        assertTrue(siedlerGame.placeInitialSettlement(new Point(5, 9), false));
        assertFalse(siedlerGame.placeInitialRoad(new Point(5, 9), new Point(10, 10)));
    }

    /**
     * Tests if its possible to build a settlement on field coordinates
     */
    @Test
    public void testBuildSettlementOnFieldCoordinates() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);

        assertFalse(siedlerGame.placeInitialSettlement(new Point(5, 10), false));
    }

    /**
     * Tests if initial settlement can be built
     */
    @Test
    public void testPlaceInitialSettlement() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Point settlementPoint = new Point(7, 7);
        assertTrue(siedlerGame.placeInitialSettlement(settlementPoint, false));
    }

    /**
     * Test if initial settlement can be built and the player get the resources for the adjacent lands
     */
    @Test
    public void testPlaceInitialSettlementWithPayout() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        //adjacent lands are: STONE, WOOL and GRAIN
        Point settlementPoint = new Point(7, 7);
        assertTrue(siedlerGame.placeInitialSettlement(settlementPoint, true));
        assertEquals(1, siedlerGame.getCurrentPlayerResourceStock(Resource.STONE));
        assertEquals(1, siedlerGame.getCurrentPlayerResourceStock(Resource.WOOL));
        assertEquals(1, siedlerGame.getCurrentPlayerResourceStock(Resource.GRAIN));
    }

    /**
     * Tests if Settlement cannot be built because the selected point is on water
     */
    @Test
    public void testSettlementCanNotBeBuiltOnWater() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);
        Point settlementPoint = new Point(9, 1);

        assertFalse(siedlerGame.placeInitialSettlement(settlementPoint, false));
    }

    /**
     * Tests build settlement on a corner that is already occupied
     */
    @Test
    public void testPlaceInitialSettlementPlaceOccupied() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);
        Point location = new Point(7, 7);
        assertTrue(siedlerGame.placeInitialSettlement(location, false));
        assertFalse(siedlerGame.placeInitialSettlement(location, false));
    }

    /**
     * Tests build settlement adjacent 3 Corners are occupied
     */
    @Test
    public void testPlaceInitialSettlement3AdjacentCornersOccupied() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        assertTrue(siedlerGame.placeInitialSettlement(new Point(7, 7), false));
        assertTrue(siedlerGame.placeInitialSettlement(new Point(6, 10), false));
        assertTrue(siedlerGame.placeInitialSettlement(new Point(8, 10), false));

        assertFalse(siedlerGame.placeInitialSettlement(new Point(7, 9), false));
    }

    /**
     * Test if settlement can't be built, because its not connected to a road
     */
    @Test
    public void testSettlementNotConnectedToARoad() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);

        bank.payoutResource(siedlerGame.getCurrentPlayer(), Resource.WOOD, 1);
        bank.payoutResource(siedlerGame.getCurrentPlayer(), Resource.CLAY, 1);
        bank.payoutResource(siedlerGame.getCurrentPlayer(), Resource.WOOL, 1);
        bank.payoutResource(siedlerGame.getCurrentPlayer(), Resource.GRAIN, 1);

        assertFalse(siedlerGame.buildSettlement(new Point(7, 7)));
    }

    /**
     * Test if settlement can't be built if the player has too few resources
     */
    @Test
    public void testSettlementNotEnoughResources() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        assertTrue(siedlerGame.placeInitialSettlement(new Point(6, 4), false));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 4), new Point(6, 6)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 6), new Point(7, 7)));

        assertFalse(siedlerGame.buildSettlement(new Point(7, 7)));

    }

    /**
     * Test if players stock is decreased after settlement was built
     */
    @Test
    public void testPlayerStockDecreaseAfterSettlementBuild() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();
        Point settlementPoint = new Point(7, 9);

        bank.payoutResource(player, Resource.CLAY, 1);
        bank.payoutResource(player, Resource.WOOD, 1);
        bank.payoutResource(player, Resource.WOOL, 1);
        bank.payoutResource(player, Resource.GRAIN, 1);

        assertTrue(siedlerGame.placeInitialSettlement(new Point(8, 6), false));
        assertTrue(siedlerGame.placeInitialRoad(new Point(8, 6), new Point(7, 7)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 7), new Point(7, 9)));

        assertTrue(siedlerGame.buildSettlement(settlementPoint));

        assertEquals(0, player.getResourceStock(Resource.CLAY));
        assertEquals(0, player.getResourceStock(Resource.WOOD));
        assertEquals(0, player.getResourceStock(Resource.WOOL));
        assertEquals(0, player.getResourceStock(Resource.GRAIN));
    }

    /**
     * Test if City cannot be built
     */
    @Test
    public void testCityCannotBeBuildWithOutResources() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Point point = new Point(9, 13);
        assertTrue(siedlerGame.placeInitialSettlement(point, false));

        assertFalse(siedlerGame.buildCity(point));
    }

    /**
     * Test if City cannot be built because Settlement is not there.
     */
    @Test
    public void testCityCannotBeBuildBecauseNoSettlement() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();

        bank.payoutResource(player, Resource.STONE, 3);
        bank.payoutResource(player, Resource.GRAIN, 2);

        assertFalse(siedlerGame.buildCity(new Point(5, 7)));
    }

    /**
     * Test if City can be built
     */
    @Test
    public void testBuildCity() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();
        Point buildPoint = new Point(5, 7);
        assertTrue(siedlerGame.placeInitialSettlement(buildPoint, false));
        bank.payoutResource(player, Resource.STONE, 3);
        bank.payoutResource(player, Resource.GRAIN, 2);

        assertTrue(siedlerGame.buildCity(buildPoint));
    }

    /**
     * Test if players Stock is decreased after City build
     */
    @Test
    public void testPlayerStockDecreaseAfterCityBuild() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();
        Point buildPoint = new Point(5, 7);

        assertTrue(siedlerGame.placeInitialSettlement(buildPoint, false));
        bank.payoutResource(player, Resource.STONE, 3);
        bank.payoutResource(player, Resource.GRAIN, 2);

        assertTrue(siedlerGame.buildCity(buildPoint));

        assertEquals(0, player.getResourceStock(Resource.STONE));
        assertEquals(0, player.getResourceStock(Resource.GRAIN));
    }

    /**
     * Test throw dice resource payout
     */
    @Test
    public void testResourcePayout() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Faction currentPlayerFaction = siedlerGame.getCurrentPlayerFaction();
        Point settlementPoint = new Point(7, 7);
        assertTrue(siedlerGame.placeInitialSettlement(settlementPoint, false));

        Map<Faction, List<Resource>> resourceMapActualWl = siedlerGame.throwDice(3);
        List<Resource> wl = resourceMapActualWl.get(currentPlayerFaction);
        assertEquals(Resource.WOOL, wl.get(0));

        Map<Faction, List<Resource>> resourceMapActualSt = siedlerGame.throwDice(4);
        List<Resource> st = resourceMapActualSt.get(currentPlayerFaction);
        assertEquals(Resource.STONE, st.get(0));

        Map<Faction, List<Resource>> resourceMapActualGr = siedlerGame.throwDice(5);
        List<Resource> gr = resourceMapActualGr.get(currentPlayerFaction);
        assertEquals(Resource.GRAIN, gr.get(0));
    }

    /**
     * Test if players Stock is increased after dice is rolled
     */
    @Test
    public void testPlayerPayout() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Player player = siedlerGame.getCurrentPlayer();
        Point settlementPoint = new Point(7, 7);
        assertTrue(siedlerGame.placeInitialSettlement(settlementPoint, false));
        siedlerGame.throwDice(3);
        siedlerGame.throwDice(4);
        siedlerGame.throwDice(5);

        assertEquals(1, player.getResourceStock(Resource.WOOL));
        assertEquals(1, player.getResourceStock(Resource.STONE));
        assertEquals(1, player.getResourceStock(Resource.GRAIN));
    }

    /**
     * Test switch player with 4 players
     */
    @Test
    public void testSwitchToNextPlayer() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);

        siedlerGame.switchToNextPlayer();
        Faction currentPlayer = siedlerGame.getCurrentPlayerFaction();
        assertEquals(Faction.BLUE, currentPlayer);

        siedlerGame.switchToNextPlayer();
        currentPlayer = siedlerGame.getCurrentPlayerFaction();
        assertEquals(Faction.GREEN, currentPlayer);

        siedlerGame.switchToNextPlayer();
        currentPlayer = siedlerGame.getCurrentPlayerFaction();
        assertEquals(Faction.YELLOW, currentPlayer);
    }

    /**
     * Test switch player with 4 players
     */
    @Test
    public void testSwitchToPreviousPlayer() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);

        for (int i = 0; i < 3; i++) {
            siedlerGame.switchToNextPlayer();
        }
        siedlerGame.switchToPreviousPlayer();

        Faction currentPlayer = siedlerGame.getCurrentPlayerFaction();
        assertEquals(Faction.GREEN, currentPlayer);
        siedlerGame.switchToPreviousPlayer();

        currentPlayer = siedlerGame.getCurrentPlayerFaction();
        assertEquals(Faction.BLUE, currentPlayer);
        siedlerGame.switchToPreviousPlayer();

        currentPlayer = siedlerGame.getCurrentPlayerFaction();
        assertEquals(Faction.RED, currentPlayer);
    }

    /**
     * Test if the siedlergame correctly initializes and returns the playerfaction list
     */
    @Test
    public void playerFactionSize() {
        for (int i = Config.MIN_NUMBER_OF_PLAYERS; i <= Faction.values().length; i++) {
            SiedlerGame siedlerGame = new SiedlerGame(3, i);
            assertEquals(i, siedlerGame.getPlayerFactions().size());
        }
    }

    /**
     * Tests trade with Bank, in case the player doesn't have any resources for the trade
     */
    @Test
    public void testTradeWithBankFourToOneWithoutPlayerResource() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 2);
        boolean isTradeSuccessful = siedlerGame.tradeWithBankFourToOne(Resource.GRAIN, Resource.CLAY);
        assertFalse(isTradeSuccessful);
    }

    /**
     * Tests trade with bank, in case the bank doesn't have any of the resources which the player requested
     */
    @Test
    public void testTradeWithBankFourToOneWithoutBankResource() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 2);
        Bank bank = getBank(siedlerGame);

        //empty bank
        bank.payoutResource(siedlerGame.getCurrentPlayer(), Resource.WOOD, 4);
        bank.setResourceStock(Resource.GRAIN, 0);

        assertFalse(siedlerGame.tradeWithBankFourToOne(Resource.WOOD, Resource.GRAIN));
    }

    /**
     * Tests trade with Bank
     */
    @Test
    public void testTradeWithBankFourToOne() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 2);
        Bank bank = getBank(siedlerGame);

        Player player = siedlerGame.getCurrentPlayer();
        bank.payoutResource(player, Resource.STONE, 4);
        bank.payoutResource(player, Resource.GRAIN, 2);

        assertTrue(siedlerGame.tradeWithBankFourToOne(Resource.STONE, Resource.CLAY));

        assertEquals(1, player.getResourceStock(Resource.CLAY));
    }

    /**
     * Tests if half of the resources are removed from the players stock and returned to the bank
     */
    @Test
    public void testDropHalfOfResource() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 2);
        Bank bank = getBank(siedlerGame);

        Player player = siedlerGame.getCurrentPlayer();
        bank.payoutResource(player, Resource.STONE, 10);
        bank.payoutResource(player, Resource.GRAIN, 10);
        bank.payoutResource(player, Resource.WOOL, 10);
        bank.payoutResource(player, Resource.CLAY, 10);
        bank.payoutResource(player, Resource.WOOD, 10);

        int playerResourceCount = player.getResourcesAsList().size();
        int bankResourceCount = bank.getResourcesAsList().size();

        siedlerGame.throwDice(7);

        assertEquals(playerResourceCount / 2, player.getResourcesAsList().size());
        assertEquals(playerResourceCount / 2 + bankResourceCount, bank.getResourcesAsList().size());
    }

    /**
     * Test when bank has no resources and one player gets resources
     */
    @Test
    public void testBankHasNotEnoughResourcesAndOnePlayerGetsResources() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);
        Player player = siedlerGame.getCurrentPlayer();
        Point buildPoint = new Point(7, 7);

        assertTrue(siedlerGame.placeInitialSettlement(buildPoint, false));
        bank.payoutResource(player, Resource.STONE, 3);
        bank.payoutResource(player, Resource.GRAIN, 2);

        assertTrue(siedlerGame.buildCity(buildPoint));

        bank.setResourceStock(Resource.WOOL, 1);

        siedlerGame.throwDice(3);

        assertEquals(1, player.getResourceStock(Resource.WOOL));
    }

    /**
     * Test when bank has no resources and more than one player gets resources
     */
    @Test
    public void testBankHasNotEnoughResourcesAndMorePlayerGetsResources() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        Bank bank = getBank(siedlerGame);

        assertTrue(siedlerGame.placeInitialSettlement(new Point(7, 7), false));
        siedlerGame.switchToNextPlayer();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(8, 4), false));
        siedlerGame.switchToNextPlayer();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(11, 9), false));
        siedlerGame.switchToNextPlayer();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(3, 9), false));

        bank.setResourceStock(Resource.WOOL, 1);

        Player playerY = siedlerGame.getCurrentPlayer();
        siedlerGame.switchToPreviousPlayer();
        Player playerG = siedlerGame.getCurrentPlayer();
        siedlerGame.switchToPreviousPlayer();
        Player playerB = siedlerGame.getCurrentPlayer();
        siedlerGame.switchToPreviousPlayer();
        Player playerR = siedlerGame.getCurrentPlayer();

        siedlerGame.throwDice(3);

        assertEquals(0, playerY.getResourceStock(Resource.WOOL));
        assertEquals(0, playerG.getResourceStock(Resource.WOOL));
        assertEquals(0, playerB.getResourceStock(Resource.WOOL));
        assertEquals(0, playerR.getResourceStock(Resource.WOOL));
    }

    /**
     * Tests if it has a winner
     */
    @Test
    public void testGetWinner() {
        SiedlerGame siedlerGame = new SiedlerGame(3, 4);
        Player player = siedlerGame.getCurrentPlayer();
        Settlement firstSettlement = new Settlement(player.getFaction());
        Settlement secondSettlement = new Settlement(player.getFaction());
        Settlement thirdSettlement = new Settlement(player.getFaction());

        player.getSettlements().add(firstSettlement);
        player.getSettlements().add(secondSettlement);
        player.getSettlements().add(thirdSettlement);

        assertEquals(siedlerGame.getWinner(), player.getFaction());
    }

    /**
     * Test if it has no Winner
     */
    @Test
    public void testGetWinnerNoWinner() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 4);
        siedlerGame.switchToNextPlayer();
        assertNull(siedlerGame.getWinner());
    }

    /**
     * Tests getWinner With Longest Roads
     */
    @Test
    public void testGetWinnerWithLongestRoad() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);
        Player player = siedlerGame.getCurrentPlayer();

        Point firstSettlementPoint = new Point(7, 7);
        Point secondSettlementPoint = new Point(7, 15);
        Point thirdSettlementPoint = new Point(5, 15);

        assertTrue(siedlerGame.placeInitialSettlement(firstSettlementPoint, false));
        assertTrue(siedlerGame.placeInitialSettlement(secondSettlementPoint, false));
        assertTrue(siedlerGame.placeInitialSettlement(thirdSettlementPoint, false));

        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 7), new Point(6, 6)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 6), new Point(6, 4)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 4), new Point(7, 3)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 3), new Point(8, 4)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(8, 4), new Point(9, 3)));

        assertEquals(siedlerGame.getWinner(), player.getFaction());
    }

    /**
     * Tests getWinner With Longest Roads
     */
    @Test
    public void testGetWinnerNoWinWithSeparateRoads() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);

        Point firstSettlementPoint = new Point(7, 7);
        Point secondSettlementPoint = new Point(6, 4);
        Point thirdSettlementPoint = new Point(4, 4);

        assertTrue(siedlerGame.placeInitialSettlement(firstSettlementPoint, false));
        assertTrue(siedlerGame.placeInitialSettlement(secondSettlementPoint, false));
        assertTrue(siedlerGame.placeInitialSettlement(thirdSettlementPoint, false));

        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 7), new Point(6, 6)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 6), new Point(6, 4)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 4), new Point(7, 3)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 3), new Point(8, 4)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(4, 4), new Point(5, 3)));

        assertNull(siedlerGame.getWinner());
    }

    /**
     * Test getWinner with one settlement and one city
     */
    @Test
    public void testGetWinnerSettlementAndCity() {
        SiedlerGame siedlerGame = new SiedlerGame(3, 4);
        Player player = siedlerGame.getCurrentPlayer();

        //add one settlement and one city
        player.getSettlements().add(new Settlement(player.getFaction()));
        player.getSettlements().add(new City(player.getFaction()));

        assertEquals(siedlerGame.getWinner(), player.getFaction());
    }

    /**
     * Tests if the longest road is not counted if another player interrupts it with a settlement
     */
    @Test
    public void testGetWinnerWithBlockedLongestRoad() {
        SiedlerGame siedlerGame = new SiedlerGame(5, 4);

        Point firstSettlementPoint = new Point(7, 7);
        Point secondSettlementPoint = new Point(7, 15);
        Point thirdSettlementPoint = new Point(5, 15);

        assertTrue(siedlerGame.placeInitialSettlement(firstSettlementPoint, false));
        assertTrue(siedlerGame.placeInitialSettlement(secondSettlementPoint, false));
        assertTrue(siedlerGame.placeInitialSettlement(thirdSettlementPoint, false));

        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 7), new Point(6, 6)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 6), new Point(6, 4)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(6, 4), new Point(7, 3)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(7, 3), new Point(8, 4)));
        assertTrue(siedlerGame.placeInitialRoad(new Point(8, 4), new Point(9, 3)));

        siedlerGame.switchToNextPlayer();
        assertTrue(siedlerGame.placeInitialSettlement(new Point(7, 3), false));
        siedlerGame.switchToPreviousPlayer();


        assertNull(siedlerGame.getWinner());
    }

    /**
     * test whether the rounding in case of odd resource numbers (7) is correct (player keeps 4, bank gets 3)
     */
    @Test
    public void testDropHalfOfResourceRounding() {
        for (Resource resource : Resource.values()) {
            SiedlerGame siedlerGame = new SiedlerGame(7, 2);
            Player player = siedlerGame.getCurrentPlayer();
            Bank bank = getBank(siedlerGame);

            assertEquals(19, bank.getResourceStock(resource));
            assertEquals(0, player.getResourcesAsList().size());

            bank.payoutResource(player, resource, 7);

            assertEquals(12, bank.getResourceStock(resource));
            assertEquals(7, player.getResourceStock(resource));
            assertEquals(7, player.getResourcesAsList().size());

            siedlerGame.throwDice(7);

            assertEquals(15, bank.getResourceStock(resource));
            assertEquals(4, player.getResourceStock(resource));
            assertEquals(4, player.getResourcesAsList().size());
        }
    }

    /**
     * make sure that the resources do not get halved if the player has less than 7 resources
     */
    @Test
    public void testDropHalfOfResourceTooFewResources() {
        for (Resource resource : Resource.values()) {
            SiedlerGame siedlerGame = new SiedlerGame(7, 2);
            Player player = siedlerGame.getCurrentPlayer();
            Bank bank = getBank(siedlerGame);

            assertEquals(19, bank.getResourceStock(resource));
            assertEquals(0, player.getResourcesAsList().size());

            bank.payoutResource(player, resource, 6);

            assertEquals(13, bank.getResourceStock(resource));
            assertEquals(6, player.getResourceStock(resource));
            assertEquals(6, player.getResourcesAsList().size());

            siedlerGame.throwDice(7);

            assertEquals(13, bank.getResourceStock(resource));
            assertEquals(6, player.getResourceStock(resource));
            assertEquals(6, player.getResourcesAsList().size());
        }
    }

    /**
     * test the payout of the resources if the stock of the bank is not sufficient
     */
    @Test
    public void testResourcePayoutBankOutOfStock() {
        SiedlerGame siedlerGame = new SiedlerGame(7, 2);
        Bank bank = getBank(siedlerGame);

        Player firstPlayer = siedlerGame.getCurrentPlayer();
        siedlerGame.placeInitialSettlement(new Point(6, 4), false);
        siedlerGame.switchToNextPlayer();

        Player secondPlayer = siedlerGame.getCurrentPlayer();
        siedlerGame.placeInitialSettlement(new Point(4, 4), false);

        bank.payoutResource(firstPlayer, Resource.WOOD, 9);
        bank.payoutResource(secondPlayer, Resource.WOOD, 9);

        assertEquals(1, bank.getResourceStock(Resource.WOOD));
        assertEquals(9, firstPlayer.getResourceStock(Resource.WOOD));
        assertEquals(9, secondPlayer.getResourceStock(Resource.WOOD));

        // both players would receive wood but the bank has not enough resources
        siedlerGame.throwDice(6);

        assertEquals(1, bank.getResourceStock(Resource.WOOD));
        assertEquals(9, firstPlayer.getResourceStock(Resource.WOOD));
        assertEquals(9, secondPlayer.getResourceStock(Resource.WOOD));

        siedlerGame.switchToNextPlayer();
        bank.payoutResource(firstPlayer, Resource.STONE, 3);
        bank.payoutResource(firstPlayer, Resource.GRAIN, 2);
        bank.payoutResource(firstPlayer, Resource.WOOL, 18);
        siedlerGame.buildCity(new Point(6, 4));

        assertEquals(1, bank.getResourceStock(Resource.WOOL));
        assertEquals(18, firstPlayer.getResourceStock(Resource.WOOL));
        assertEquals(0, secondPlayer.getResourceStock(Resource.WOOL));

        // only the first player receives wool
        siedlerGame.throwDice(3);

        assertEquals(0, bank.getResourceStock(Resource.WOOL));
        assertEquals(19, firstPlayer.getResourceStock(Resource.WOOL));
        assertEquals(0, secondPlayer.getResourceStock(Resource.WOOL));
    }

    /**
     * Access the bank value of a siedler game (discussed with Mr. Bleisch)
     *
     * @param siedlerGame siedler game that contains current bank
     * @return Bank instance of siedler game
     */
    private Bank getBank(SiedlerGame siedlerGame) {
        Bank bank = null;
        try {
            Field field = siedlerGame.getClass().getDeclaredField("bank");
            field.setAccessible(true);
            bank = (Bank) field.get(siedlerGame);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        assertNotNull(bank);
        return bank;
    }

}
