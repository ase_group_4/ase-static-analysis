package ch.zhaw.archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;

import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

@AnalyzeClasses(packages = "ch.zhaw")
public class ArchUnitSampleTest {

    @ArchTest
    private final ArchRule no_classes_should_throw_generic_exceptions = NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;

    @ArchTest
    public void no_methods_should_declare_throwable_of_type_exception(JavaClasses classes) {
        ArchRuleDefinition.noMethods().should().declareThrowableOfType(Exception.class).check(classes);
    }

}
