package ch.zhaw.catan.io;

/**
 * All available commands for the user
 *
 * @author Nils, Pascal
 * @version 01.12.2020
 */
public enum Command {
    TRADE,
    BUILD_ROAD,
    BUILD_SETTLEMENT,
    BUILD_CITY,
    SHOW_RESOURCES,
    END_TURN,
    QUIT
}
