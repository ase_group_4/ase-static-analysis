package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;

/**
 * Model class for {@code City}
 * This class stores the faction which owns the {@code City}
 *
 * @author Pascal, Ridvan, Nils
 * @version 04.12.2020
 */
public class City extends Settlement {
    private static final int RESOURCES_PER_CITY = 2;
    private static final int POINTS_PER_CITY = 2;

    /**
     * Create a new instance of {@code City} and set the faction
     *
     * @param faction faction which owns the city
     */
    public City(Config.Faction faction) {
        super(faction, RESOURCES_PER_CITY, POINTS_PER_CITY);
    }

    /**
     * returns the faction in uppercase
     *
     * @return faction in uppercase
     */
    @Override
    public String toString() {
        return super.toString().toUpperCase();
    }
}
