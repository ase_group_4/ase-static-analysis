package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;

/**
 * Base class for all structures which can be built
 *
 * @author Nils
 * @version 04.12.2020
 */
public abstract class Structure {
    private final Config.Faction faction;

    /**
     * assigns the parameter {@code faction} to the attribute {@code this.faction}
     *
     * @param faction the faction which owns the structure
     */
    public Structure(Config.Faction faction) {
        this.faction = faction;
    }

    /**
     * returns the faction
     *
     * @return faction
     */
    @Override
    public String toString() {
        return faction.toString();
    }

    /**
     * Get faction which owns the structure
     *
     * @return faction of the structure owner
     */
    public Config.Faction getFaction() {
        return faction;
    }
}
