package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class can be used for objects that need to save the stock of resources
 *
 * @author Pascal
 * @version 01.12.2020
 */
public abstract class ResourceOwner {
    private final Map<Config.Resource, Integer> resources = new HashMap<>();

    /**
     * The constructor of {@code ResourceOwner} initializes the resources and sets the stock of each to zero
     */
    public ResourceOwner() {
        for (Config.Resource resource : Config.Resource.values()) {
            resources.put(resource, 0);
        }
    }

    /**
     * Override the current stock of a resource
     *
     * @param resource the resource of which the stock will be changed
     * @param stock    the new stock
     */
    public void setResourceStock(Config.Resource resource, int stock) {
        resources.put(resource, stock);
    }

    /**
     * Get the current stock of a resource
     *
     * @param resource the resource of which you want the stock
     * @return the stock of the passed resource
     */
    public int getResourceStock(Config.Resource resource) {
        return resources.get(resource);
    }


    /**
     * Increase the stock of a resource by the specified amount
     *
     * @param resource the resource of which you want to change the stock
     * @param amount   the amount by which you want to increase the stock
     */
    protected void increaseResourceStock(Config.Resource resource, int amount) {
        setResourceStock(resource, getResourceStock(resource) + amount);
    }

    /**
     * Decrease the stock of a resource by the specified amount
     *
     * @param resource the resource of which you want to change the stock
     * @param amount   the amount by which you want to decrease the stock
     */
    protected void decreaseResourceStock(Config.Resource resource, int amount) {
        setResourceStock(resource, getResourceStock(resource) - amount);
    }

    /**
     * Return the resources of the resource owner as list
     *
     * @return list of all resources
     */
    public List<Config.Resource> getResourcesAsList() {
        List<Config.Resource> resourcesAsList = new ArrayList<>();
        for (Map.Entry<Config.Resource, Integer> entry : resources.entrySet()) {
            for (int i = 0; i < entry.getValue(); i++) {
                resourcesAsList.add(entry.getKey());
            }
        }
        return resourcesAsList;
    }
}
