package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Model class for {@code Player}
 * This class stores the faction of the player, his settlements, roads and resources
 *
 * @author Pascal, Nils
 * @version 01.12.2020
 */
public class Player extends ResourceOwner {
    private final Config.Faction faction;
    private final List<Settlement> settlements = new ArrayList<>();
    private final List<Road> roads = new ArrayList<>();

    /**
     * Create a new instance of {@code Player} and set the faction
     *
     * @param faction faction of the player
     */
    public Player(Config.Faction faction) {
        this.faction = faction;
    }

    /**
     * Get the roads of the player
     *
     * @return roads owned by the player
     */
    public List<Road> getRoads() {
        return roads;
    }

    /**
     * Get the settlements of the player
     *
     * @return settlements owned by the player
     */
    public List<Settlement> getSettlements() {
        return settlements;
    }

    /**
     * Get the faction of the player
     *
     * @return the faction of the player
     */
    public Config.Faction getFaction() {
        return faction;
    }

    /**
     * count the number of settlements
     *
     * @return the number of settlements
     */
    public long getNumberOfSettlements() {
        return settlements.stream().filter(settlement -> settlement.getClass() == Settlement.class).count();
    }

    /**
     * count the number of cities
     *
     * @return the number of cities
     */
    public long getNumberOfCities() {
        return settlements.stream().filter(settlement -> settlement.getClass() == City.class).count();
    }
}
