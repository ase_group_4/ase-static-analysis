package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;


/**
 * Model class for {@code Settlement}
 * This class stores the faction which owns the {@code Settlement}
 *
 * @author Pascal, Ridvan, Nils
 * @version 03.12.2020
 */
public class Settlement extends Structure {
    private static final int RESOURCES_PER_SETTLEMENT = 1;
    private static final int POINTS_PER_SETTLEMENT = 1;

    private final int resourcesForSettlement;
    private final int pointsForSettlement;

    /**
     * Create a new instance of {@code Settlement} and set the owner
     *
     * @param faction faction which owns the settlement
     */
    public Settlement(Config.Faction faction) {
        this(faction, RESOURCES_PER_SETTLEMENT, POINTS_PER_SETTLEMENT);
    }

    /**
     * Constructor in order to set the amount of points per settlement
     *
     * @param faction                owner of the settlement
     * @param resourcesForSettlement amount of resources the player will get for one settlement
     * @param pointsForSettlement    amount of points the player will get for one settlement
     */
    protected Settlement(Config.Faction faction, int resourcesForSettlement, int pointsForSettlement) {
        super(faction);
        this.resourcesForSettlement = resourcesForSettlement;
        this.pointsForSettlement = pointsForSettlement;
    }

    /**
     * get the amount of resources the player will get for this settlement
     *
     * @return amount of resources
     */
    public int getResourcesForSettlement() {
        return resourcesForSettlement;
    }

    /**
     * get the amount of points that count towards the players total in order to win
     *
     * @return points for this settlement
     */
    public int getPointsForSettlement() {
        return pointsForSettlement;
    }
}
