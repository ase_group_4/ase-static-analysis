package ch.zhaw.catan.model;

import ch.zhaw.catan.Config;

import java.awt.*;
import java.util.Objects;

/**
 * Model class for {@code Road}
 * This class stores the faction which owns the {@code Road} and the location of the {@code Road}
 *
 * @author Pascal, Nils
 * @version 01.12.2020
 */
public class Road extends Structure {
    private final Point from;
    private final Point to;

    /**
     * Create a new instance of {@code Road} and set the owner, from and to point
     *
     * @param faction faction which owns the road
     * @param from    start point of the road
     * @param to      end point of the road
     */
    public Road(Config.Faction faction, Point from, Point to) {
        super(faction);
        this.from = from;
        this.to = to;
    }

    /**
     * Get from point of the road
     *
     * @return from point of the road
     */
    public Point getFrom() {
        return from;
    }

    /**
     * Get to point of the road
     *
     * @return to point of the road
     */
    public Point getTo() {
        return to;
    }

    /**
     * Override equals
     *
     * @param o object to compare to
     * @return if the objects are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Road road = (Road) o;
        return Objects.equals(from, road.from) &&
                Objects.equals(to, road.to);
    }

    /**
     * Override hash
     *
     * @return hash of instance
     */
    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }
}
