package ch.zhaw.catan.service;

import ch.zhaw.catan.Config;
import ch.zhaw.catan.SiedlerBoard;
import ch.zhaw.catan.model.*;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible for building structures.
 * any checks and needed updates are performed in the BuildService
 *
 * @author Nils, Ridvan, Pascal
 * @version 03.12.2020
 */
public class BuildService {
    private final SiedlerBoard siedlerBoard;
    private final Bank bank;

    /**
     * assigns the parameter {@code siedlerBoard} to the attribute {@code this.siedlerBoard}
     *
     * @param siedlerBoard instance of the game board
     * @param bank         instance of the bank
     */
    public BuildService(SiedlerBoard siedlerBoard, Bank bank) {
        this.siedlerBoard = siedlerBoard;
        this.bank = bank;
    }

    /**
     * this method is used to build either a settlement or a city
     * all the required criteria are being checked in this method
     *
     * @param player           current player
     * @param structure        which needs to be built
     * @param position         of the structure which needs to be built
     * @param initialStructure if true the building does not cost any resources
     * @return true, if the structure was built successful
     */
    public boolean buildStructure(Player player, Config.Structure structure, Point position, boolean initialStructure) {
        return buildStructure(player, structure, position, null, initialStructure);
    }

    /**
     * this method is used to build any structure(Settlement, City or Road)
     * all the required criteria are being checked in this method
     *
     * @param player           currentPlayer
     * @param structure        which needs to be built
     * @param firstPoint       of the structure which needs to be built
     * @param secondPoint      of the structure which needs to be built (only provided for roads)
     * @param initialStructure if true the building does not cost any resources
     * @return true, if the structure was built successful
     */
    public boolean buildStructure(Player player, Config.Structure structure, Point firstPoint, Point secondPoint, boolean initialStructure) {
        Structure structureToBuild = null;
        if (positionExists(firstPoint, secondPoint) &&
                structureAvailableForPlayer(player, structure) &&
                (initialStructure || hasEnoughResourcesToBuild(player, structure)) &&
                isValidLocation(player.getFaction(), structure, firstPoint, secondPoint, initialStructure)) {
            switch (structure) {
                case SETTLEMENT:
                    structureToBuild = new Settlement(player.getFaction());
                    siedlerBoard.setCorner(firstPoint, (Settlement) structureToBuild);
                    player.getSettlements().add((Settlement) structureToBuild);
                    break;
                case CITY:
                    player.getSettlements().remove(siedlerBoard.getCorner(firstPoint));
                    structureToBuild = new City(player.getFaction());
                    siedlerBoard.setCorner(firstPoint, (City) structureToBuild);
                    player.getSettlements().add((City) structureToBuild);
                    break;
                case ROAD:
                    structureToBuild = new Road(player.getFaction(), firstPoint, secondPoint);
                    siedlerBoard.setEdge(firstPoint, secondPoint, (Road) structureToBuild);
                    player.getRoads().add((Road) structureToBuild);
                    break;
            }
            if (!initialStructure) {
                updateStockOfPlayerAfterBuild(player, structure.getCostsAsMap());
            }
        }
        return structureToBuild != null;
    }

    /**
     * updates the resources of the current player after building a structure
     *
     * @param player    current player
     * @param resources the map of the required resource types and their number
     */
    private void updateStockOfPlayerAfterBuild(Player player, Map<Config.Resource, Long> resources) {
        for (Map.Entry<Config.Resource, Long> entry : resources.entrySet()) {
            bank.depositResource(player, entry.getKey(), entry.getValue().intValue());
        }
    }

    /**
     * validates the location which the user provided based on which structure he is trying to build
     *
     * @param faction          of the current player
     * @param structure        which needs to be built
     * @param firstPoint       of the structure which needs to be built
     * @param secondPoint      of the structure which needs to be built (only provided for roads)
     * @param initialStructure if true the structure is being built during Phase II
     * @return true, if the location selected by the user is valid for building the requested structure
     */
    private boolean isValidLocation(Config.Faction faction, Config.Structure structure, Point firstPoint, Point secondPoint, boolean initialStructure) {
        boolean validLocation = false;
        if (isOnMainland(firstPoint) && isOnMainland(secondPoint)) {
            switch (structure) {
                case ROAD:
                    validLocation = isValidLocationForRoad(faction, firstPoint, secondPoint);
                    break;
                case SETTLEMENT:
                    validLocation = isValidLocationForSettlement(faction, firstPoint, initialStructure);
                    break;
                case CITY:
                    validLocation = isValidLocationForCity(faction, firstPoint);
                    break;
            }
        }
        return validLocation;
    }

    /**
     * Check if the resource is on the mainland
     *
     * @param point location
     * @return true if at least one of the adjacent lands is not water
     */
    private boolean isOnMainland(Point point) {
        return point == null || siedlerBoard.getFields(point).stream().anyMatch(land -> !land.equals(Config.Land.WATER));
    }

    /**
     * this method checks if the provided location is valid for a road
     *
     * @param faction     of the current player
     * @param firstPoint  of the structure which needs to be built
     * @param secondPoint of the structure which needs to be built
     * @return true, if the selected location is valid for a road
     */
    private boolean isValidLocationForRoad(Config.Faction faction, Point firstPoint, Point secondPoint) {
        boolean validLocation = false;
        Road road = siedlerBoard.getEdge(firstPoint, secondPoint);
        if (road == null) {
            validLocation = isValidConnectionPointForRoad(faction, firstPoint) ||
                    isValidConnectionPointForRoad(faction, secondPoint);
        }
        return validLocation;
    }

    /**
     * this method checks if the provided point is connected to a settlement, city or road of the current player
     *
     * @param faction of the current player
     * @param point   of the road for which we check if it is connected to any owned structure
     * @return true, if the provided point is connected to an owned structure
     */
    private boolean isValidConnectionPointForRoad(Config.Faction faction, Point point) {
        Settlement settlement = siedlerBoard.getCorner(point);
        return settlement != null && settlement.getFaction().equals(faction) ||
                settlement == null && connectedToOwnedRoad(faction, point);
    }

    /**
     * checks if the user selected a valid spot for building a settlement
     *
     * @param faction          of the current player
     * @param position         where the settlement should be built
     * @param initialStructure if true the settlement does not need to be connected to a road
     * @return true, if a settlement can be built at the provided position
     */
    private boolean isValidLocationForSettlement(Config.Faction faction, Point position, boolean initialStructure) {
        if (siedlerBoard.getCorner(position) == null) {
            List<Settlement> neighbours = siedlerBoard.getNeighboursOfCorner(position);
            if (neighbours.isEmpty()) {
                return connectedToOwnedRoad(faction, position) || initialStructure;
            }
        }
        return false;
    }

    /**
     * checks if the provided point is connected to a road which is owned by {@code faction}
     *
     * @param faction of the current player
     * @param point   which needs to be checked
     * @return true, if the provided point is connected to a road which is owned by {@code faction}
     */
    private boolean connectedToOwnedRoad(Config.Faction faction, Point point) {
        return siedlerBoard.getAdjacentEdges(point).stream().anyMatch(road -> faction.equals(road.getFaction()));
    }

    /**
     * checks if the provided point is a settlement owned by {@code faction} which can be upgraded to a city
     *
     * @param faction  of the current player
     * @param position which needs to  be checked
     * @return true, if it is a valid location for a city
     */
    private boolean isValidLocationForCity(Config.Faction faction, Point position) {
        Settlement settlement = siedlerBoard.getCorner(position);
        return settlement != null && settlement.getFaction().equals(faction) && settlement.getClass() != City.class;
    }

    /**
     * checks if the current player has exceeded his limit for the structure he wants to build
     *
     * @param player    current player
     * @param structure which needs to be built
     * @return true, if the structure is available for the {@code player}
     */
    private boolean structureAvailableForPlayer(Player player, Config.Structure structure) {
        switch (structure) {
            case CITY:
                return player.getNumberOfCities() < structure.getStockPerPlayer();
            case SETTLEMENT:
                return player.getNumberOfSettlements() < structure.getStockPerPlayer();
            case ROAD:
                return player.getRoads().size() < structure.getStockPerPlayer();
            default:
                return false;
        }
    }

    /**
     * checks if the user has enough resources to build the selected structure
     *
     * @param player    current player
     * @param structure which needs to be built
     * @return true, if the {@code player} has enough resources to build the structure
     */
    private boolean hasEnoughResourcesToBuild(Player player, Config.Structure structure) {
        Map<Config.Resource, Long> neededResources = structure.getCostsAsMap();
        for (Map.Entry<Config.Resource, Long> resource : neededResources.entrySet()) {
            if (player.getResourceStock(resource.getKey()) < resource.getValue()) {
                return false;
            }
        }
        return true;
    }

    /**
     * checks if the provided location exists on the board
     *
     * @param firstPoint  of the structure which needs to be built
     * @param secondPoint of the structure which needs to be built (only provided for roads)
     * @return true, if the location exists on the board
     */
    private boolean positionExists(Point firstPoint, Point secondPoint) {
        if (secondPoint != null) {
            return siedlerBoard.hasEdge(firstPoint, secondPoint);
        }
        return siedlerBoard.hasCorner(firstPoint);
    }
}
