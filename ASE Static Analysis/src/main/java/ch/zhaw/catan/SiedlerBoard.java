package ch.zhaw.catan;

import ch.zhaw.catan.model.Road;
import ch.zhaw.catan.model.Settlement;
import ch.zhaw.hexboard.HexBoard;
import ch.zhaw.hexboard.Label;

import java.awt.*;
import java.util.Map;

/**
 * This class extends the {@code HexBoard} with the generic Types Land, Settlement, Road, and String
 *
 * @author Nils, Alina, Pascal
 * @version 01.12.2020
 */
public class SiedlerBoard extends HexBoard<Config.Land, Settlement, Road, String> {
    private final SiedlerBoardTextView siedlerBoardTextView;

    /**
     * initialize the SiedlerBoard with the standard land placement
     */
    public SiedlerBoard() {
        super();

        Map<Point, Config.Land> landPlacement = Config.getStandardLandPlacement();
        for (Map.Entry<Point, Config.Land> field : landPlacement.entrySet()) {
            addField(field.getKey(), field.getValue());
        }

        Map<Point, Integer> labels = Config.getStandardDiceNumberPlacement();
        siedlerBoardTextView = new SiedlerBoardTextView(this);

        for (Map.Entry<Point, Integer> entry : labels.entrySet()) {
            String label = String.format("%02d", entry.getValue()); // add leading spaces
            siedlerBoardTextView.setLowerFieldLabel(entry.getKey(), new Label(label.charAt(0), label.charAt(1)));
        }
    }

    /**
     * Returns the instance of the siedlerBoardTextView
     *
     * @return siedlerBoardTextView
     */
    public SiedlerBoardTextView getSiedlerBoardTextView() {
        return siedlerBoardTextView;
    }
}
