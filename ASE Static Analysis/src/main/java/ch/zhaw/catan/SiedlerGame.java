package ch.zhaw.catan;

import ch.zhaw.catan.io.Text;
import ch.zhaw.catan.model.Bank;
import ch.zhaw.catan.model.Player;
import ch.zhaw.catan.model.Road;
import ch.zhaw.catan.model.Settlement;
import ch.zhaw.catan.service.BuildService;

import java.awt.*;
import java.util.List;
import java.util.*;


/**
 * This class performs all actions related to modifying the game state.
 *
 * <p>For example, calling the method {@link SiedlerGame#throwDice(int)}
 * will do the payout of the resources to the players according to
 * the payout rules of the game which take into account factors like
 * the amount of resources requested of a certain type, the number of players
 * requesting it, the amount of resources available in the bank and
 * the settlement types.</p>
 *
 * @author Alina, Ridvan, Pascal, Nils
 * @version 03.12.2020
 */
public class SiedlerGame {
    private static final int DICE_THROW_DISCARD_RESOURCES_VALUE = 7;
    private static final int DICE_THROW_DISCARD_RESOURCES_THRESHOLD = 7;

    private static final int LONGEST_ROAD_MIN_LENGTH = 5;
    private static final int LONGEST_ROAD_POINTS = 2;

    private static final int MIN_WIN_POINTS = 3;
    private final int winPoints;

    private final SiedlerBoard siedlerBoard;
    private final Bank bank;
    private final BuildService buildService;
    private final Map<Config.Faction, Player> players;

    private Config.Faction longestRoadFaction;
    private Config.Faction currentPlayer;

    /**
     * Constructs a SiedlerGame game state object.
     *
     * @param winPoints    the number of points required to win the game
     * @param playersCount the number of players
     * @throws IllegalArgumentException if winPoints is lower than
     *                                  three or players is not between two and four
     */
    public SiedlerGame(int winPoints, int playersCount) {
        if (playersCount > Config.Faction.values().length || playersCount < Config.MIN_NUMBER_OF_PLAYERS || winPoints < MIN_WIN_POINTS) {
            throw new IllegalArgumentException(Text.INVALID_INPUT.getText());
        }
        this.winPoints = winPoints;
        siedlerBoard = new SiedlerBoard();
        bank = new Bank();
        buildService = new BuildService(siedlerBoard, bank);
        players = new HashMap<>();

        for (int i = 0; i < playersCount; i++) {
            Config.Faction faction = Config.Faction.values()[i];
            players.put(faction, new Player(faction));
        }
        currentPlayer = Config.Faction.RED;
    }

    /**
     * Switches to the next player in the defined sequence of players.
     */
    public void switchToNextPlayer() {
        int currentIndex = currentPlayer.ordinal();
        if (++currentIndex >= players.size()) {
            currentIndex = 0;
        }
        currentPlayer = Config.Faction.values()[currentIndex];
    }

    /**
     * Switches to the previous player in the defined sequence of players.
     */
    public void switchToPreviousPlayer() {
        int currentIndex = currentPlayer.ordinal();
        if (--currentIndex < 0) {
            currentIndex = players.size() - 1;
        }
        currentPlayer = Config.Faction.values()[currentIndex];
    }

    /**
     * Returns the {@link Config.Faction}s of the active players.
     *
     * <p>The order of the player's factions in the list must
     * correspond to the oder in which they play.
     * Hence, the player that sets the first settlement must be
     * at position 0 in the list etc.
     *
     * <strong>Important note:</strong> The list must contain the
     * factions of active players only.</p>
     *
     * @return the list with player's factions
     */
    public List<Config.Faction> getPlayerFactions() {
        return Arrays.asList(Config.Faction.values()).subList(0, players.size());
    }


    /**
     * Returns the game board.
     *
     * @return the game board
     */
    public SiedlerBoard getBoard() {
        return siedlerBoard;
    }

    /**
     * Returns the {@link Config.Faction} of the current player.
     *
     * @return the faction of the current player
     */
    public Config.Faction getCurrentPlayerFaction() {
        return currentPlayer;
    }

    /**
     * Returns the {@link Player} of the current turn
     *
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return players.get(currentPlayer);
    }

    /**
     * Gets the current amount of resources of the specified type
     * in the current players' stock (hand).
     *
     * @param resource the resource type
     * @return the amount of resources of this type
     */
    public int getCurrentPlayerResourceStock(Config.Resource resource) {
        return players.get(currentPlayer).getResourceStock(resource);
    }

    /**
     * Places a settlement in the founder's phase (phase II) of the game.
     *
     * <p>The placement does not cost any resources. If payout is
     * set to true, one resource per adjacent field is taken from the
     * bank and added to the players' stock of resources.</p>
     *
     * @param position the position of the settlement
     * @param payout   true, if the player shall get the resources of the surrounding fields
     * @return true, if the placement was successful
     */
    public boolean placeInitialSettlement(Point position, boolean payout) {
        if (buildSettlement(position, true)) {
            if (payout) {
                Player player = players.get(currentPlayer);
                Settlement settlement = siedlerBoard.getCorner(position);
                for (Config.Land land : siedlerBoard.getFields(position)) {
                    if (land.getResource() != null) {
                        bank.payoutResource(player, land.getResource(), settlement.getResourcesForSettlement());
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Places a road in the founder's phase (phase II) of the game.
     * The placement does not cost any resources.
     *
     * @param roadStart position of the start of the road
     * @param roadEnd   position of the end of the road
     * @return true, if the placement was successful
     */
    public boolean placeInitialRoad(Point roadStart, Point roadEnd) {
        return buildRoad(roadStart, roadEnd, true);
    }

    /**
     * This method takes care of the payout of the resources to the players
     * according to the payout rules of the game. If a player does not get resources,
     * the list for this players' faction is empty.
     *
     * <p>The payout rules of the game take into account factors like
     * the amount of resources of a certain type, the number of players
     * requesting resources of this type, the amount of resources available
     * in the bank and the settlement types.</p>
     *
     * @param dicethrow the result of the dice throw
     * @return the resources that have been paid to the players
     */
    public Map<Config.Faction, List<Config.Resource>> throwDice(int dicethrow) {
        Map<Config.Faction, List<Config.Resource>> resourceMap = new HashMap<>();
        for (int i = 0; i < players.size(); i++) {
            resourceMap.put(Config.Faction.values()[i], new ArrayList<>());
        }

        if (dicethrow == DICE_THROW_DISCARD_RESOURCES_VALUE) {
            discardHalfOfResources();
        } else {
            getResourcesForFaction(resourceMap, dicethrow);
            checkBankStock(resourceMap);
        }

        for (Map.Entry<Config.Faction, List<Config.Resource>> entry : resourceMap.entrySet()) {
            for (Config.Resource resource : entry.getValue()) {
                bank.payoutResource(players.get(entry.getKey()), resource, 1);
            }
        }

        return resourceMap;
    }

    /**
     * returns half of the resources, of all players which have more than 7 resources, to the bank
     * the resources are picked randomly
     */
    private void discardHalfOfResources() {
        for (Player player : players.values()) {
            List<Config.Resource> resources = player.getResourcesAsList();
            if (resources.size() >= DICE_THROW_DISCARD_RESOURCES_THRESHOLD) {
                Collections.shuffle(resources);
                for (int i = 0; i < resources.size() / 2; i++) {
                    bank.depositResource(player, resources.get(i), 1);
                }
            }
        }
    }

    /**
     * determines which faction gets how many resources based on the dicethrow value
     *
     * @param resourceMap map of the player factions and a list of resources
     * @param dicethrow   number of the dice throw
     */
    private void getResourcesForFaction(Map<Config.Faction, List<Config.Resource>> resourceMap, int dicethrow) {
        for (Map.Entry<Point, Integer> placement : Config.getStandardDiceNumberPlacement().entrySet()) {
            if (placement.getValue().equals(dicethrow)) {
                Config.Resource resource = siedlerBoard.getField(placement.getKey()).getResource();
                if (resource != null) {
                    List<Settlement> corners = siedlerBoard.getCornersOfField(placement.getKey());
                    for (Settlement settlement : corners) {
                        for (int i = 0; i < settlement.getResourcesForSettlement(); i++) {
                            resourceMap.get(settlement.getFaction()).add(resource);
                        }
                    }
                }
            }
        }
    }

    /**
     * checks if there are enough resources in the bank to pay all the players
     * if not and its only one player, the player gets the remaining resources of the bank stock
     * if there are two players and the bank does not have enough resources no one gets anything
     *
     * @param resourceMap map of the player and which resources he would get
     */
    private void checkBankStock(Map<Config.Faction, List<Config.Resource>> resourceMap) {
        for (Config.Resource resource : Config.Resource.values()) {
            int required = 0;
            List<Config.Faction> factions = new ArrayList<>();

            for (var entry : resourceMap.entrySet()) {
                int count = (int) entry.getValue().stream().filter(r -> r.equals(resource)).count();
                if (count > 0) {
                    required += count;
                    factions.add(entry.getKey());
                }
            }

            if (required > bank.getResourceStock(resource)) {
                if (factions.size() > 1) {
                    for (Config.Faction faction : factions) {
                        List<Config.Resource> resources = resourceMap.get(faction);
                        while (resources.contains(resource)) {
                            resources.remove(resource);
                        }
                    }
                } else if (!factions.isEmpty()) {
                    List<Config.Resource> resources = resourceMap.get(factions.get(0));
                    for (int i = 0; i < required - bank.getResourceStock(resource); i++) {
                        resources.remove(resource);
                    }
                }
            }
        }
    }

    /**
     * Builds a settlement at the specified position on the board.
     *
     * <p>The settlement can be built if:
     * <ul>
     * <li> the player has the resource cards required</li>
     * <li> a settlement to place on the board</li>
     * <li> the specified position meets the build rules for settlements</li>
     * </ul>
     *
     * @param position the position of the settlement
     * @return true, if the placement was successful
     */
    public boolean buildSettlement(Point position) {
        return buildSettlement(position, false);
    }

    /**
     * Builds a settlement at the specified position on the board.
     *
     * <p>The settlement can be built if:
     * <ul>
     * <li> the player has the resource cards required</li>
     * <li> a settlement to place on the board</li>
     * <li> the specified position meets the build rules for settlements</li>
     * </ul>
     *
     * @param position          the position of the settlement
     * @param initialSettlement if it is the initial road in (phase II)
     * @return true, if the placement was successful
     */
    private boolean buildSettlement(Point position, boolean initialSettlement) {
        return buildService.buildStructure(players.get(currentPlayer), Config.Structure.SETTLEMENT, position, initialSettlement);
    }

    /**
     * Builds a city at the specified position on the board.
     *
     * <p>The city can be built if:
     * <ul>
     * <li> the player has the resource cards required</li>
     * <li> a city to place on the board</li>
     * <li> the specified position meets the build rules for cities</li>
     * </ul>
     *
     * @param position the position of the city
     * @return true, if the placement was successful
     */
    public boolean buildCity(Point position) {
        return buildService.buildStructure(players.get(currentPlayer), Config.Structure.CITY, position, false);
    }

    /**
     * Builds a road at the specified position on the board.
     *
     * <p>The road can be built if:
     * <ul>
     * <li> the player has the resource cards required</li>
     * <li> a road to place on the board</li>
     * <li> the specified position meets the build rules for roads</li>
     * </ul>
     *
     * @param roadStart the position of the start of the road
     * @param roadEnd   the position of the end of the road
     * @return true, if the placement was successful
     */
    public boolean buildRoad(Point roadStart, Point roadEnd) {
        return buildRoad(roadStart, roadEnd, false);
    }

    /**
     * Builds a road at the specified position on the board.
     *
     * <p>The road can be built if:
     * <ul>
     * <li> the player has the resource cards required</li>
     * <li> a road to place on the board</li>
     * <li> the specified position meets the build rules for roads</li>
     * </ul>
     *
     * @param roadStart   the position of the start of the road
     * @param roadEnd     the position of the end of the road
     * @param initialRoad if it is the initial road in (phase II)
     * @return true, if the placement was successful
     */
    private boolean buildRoad(Point roadStart, Point roadEnd, boolean initialRoad) {
        return buildService.buildStructure(players.get(currentPlayer), Config.Structure.ROAD, roadStart, roadEnd, initialRoad);
    }


    /**
     * Trades in {@value Bank#REQUIRED_RESOURCES_FOR_TRADE} resources of the
     * offered type for {@value Bank#RETURNED_RESOURCES_FOR_TRADE} resource of the wanted type.
     *
     * @param offer offered type
     * @param want  wanted type
     * @return true, if player and bank had enough resources and the trade was successful
     */
    public boolean tradeWithBankFourToOne(Config.Resource offer, Config.Resource want) {
        Player player = players.get(currentPlayer);

        if (player.getResourceStock(offer) < Bank.REQUIRED_RESOURCES_FOR_TRADE ||
                bank.getResourceStock(want) < Bank.RETURNED_RESOURCES_FOR_TRADE) {
            return false;
        }

        bank.payoutResource(player, want, Bank.RETURNED_RESOURCES_FOR_TRADE);
        bank.depositResource(player, offer, Bank.REQUIRED_RESOURCES_FOR_TRADE);

        return true;
    }

    /**
     * Returns the winner of the game, if any.
     *
     * @return the winner of the game or null, if there is no winner (yet)
     */
    public Config.Faction getWinner() {
        Player player = players.get(currentPlayer);

        updateLongestRoad();

        int points = 0;

        if (player.getFaction().equals(longestRoadFaction)) {
            points += LONGEST_ROAD_POINTS;
        }

        for (Settlement settlement : player.getSettlements()) {
            points += settlement.getPointsForSettlement();
        }

        if (points >= winPoints) {
            return player.getFaction();
        }

        return null;
    }

    /**
     * Check which faction has the longest road
     */
    private void updateLongestRoad() {
        List<Config.Faction> playerWithLongestRoad = new ArrayList<>();
        int longestRoadLength = 0;

        for (Player player : players.values()) {
            int length = getLongestRoad(player);
            if (length >= LONGEST_ROAD_MIN_LENGTH && length >= longestRoadLength) {
                if (length > longestRoadLength) {
                    longestRoadLength = length;
                    playerWithLongestRoad.clear();
                }
                playerWithLongestRoad.add(player.getFaction());
            }
        }

        // reset longest road
        if (playerWithLongestRoad.isEmpty() || !playerWithLongestRoad.contains(longestRoadFaction)) {
            longestRoadFaction = null;
        }

        // only change if exactly one player has the longest road
        if (playerWithLongestRoad.size() == 1) {
            longestRoadFaction = playerWithLongestRoad.get(0);
        }
    }

    /**
     * Get the length of the longest road for a player
     *
     * @param player whose players roads should be checked
     * @return length of the longest road
     */
    private int getLongestRoad(Player player) {
        List<Road> roads = new ArrayList<>();
        for (Road road : player.getRoads()) {
            roads.add(road);
            roads.add(invertRoad(road)); // add same road but swap start and end
        }
        return getLongestRoad(roads, null);
    }

    /**
     * Invert a road (to and from are swapped)
     *
     * @param road road to invert
     * @return inverted road
     */
    private Road invertRoad(Road road) {
        return new Road(road.getFaction(), road.getTo(), road.getFrom());
    }

    /**
     * Return the length of the longest consecutive road without another factions settlement in between
     *
     * @param roads        remaining roads to check
     * @param previousRoad previous road (optional)
     * @return length of the longest road
     */
    private int getLongestRoad(List<Road> roads, Road previousRoad) {
        // check if road leads to another factions settlement
        if (previousRoad != null) {
            Settlement destination = siedlerBoard.getCorner(previousRoad.getTo());
            if (destination != null && !destination.getFaction().equals(previousRoad.getFaction())) {
                return 0;
            }
        }
        int maxLength = 0;
        for (Road road : roads) {
            if (previousRoad == null || previousRoad.getTo().equals(road.getFrom())) {
                List<Road> remainingRoads = new ArrayList<>(roads);
                remainingRoads.remove(road);
                remainingRoads.remove(invertRoad(road)); // same road but from and to are swapped
                int length = getLongestRoad(remainingRoads, road) + 1; // recursive call + current road
                if (length > maxLength) {
                    maxLength = length;
                }
            }
        }
        return maxLength;
    }

    /**
     * Places the thief on the specified field and steals a random resource card (if
     * the player has such cards) from a random player with a settlement at that
     * field (if there is a settlement) and adds it to the resource cards of the
     * current player.
     *
     * @param field the field on which to place the thief
     * @return false, if the specified field is not a field or the thief cannot be
     * placed there (e.g., on water)
     */
    public boolean placeThiefAndStealCard(Point field) {
        // implemented longest road instead
        return false;
    }

}
