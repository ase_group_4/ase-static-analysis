package ch.zhaw.catan;

import ch.zhaw.catan.model.Road;
import ch.zhaw.catan.model.Settlement;
import ch.zhaw.hexboard.HexBoardTextView;

/**
 * This class extends the {@code HexBoardTextView} with the generic Types Land, Settlement, Road, and String
 *
 * @author Pascal
 * @version 01.12.2020
 */
public class SiedlerBoardTextView extends HexBoardTextView<Config.Land, Settlement, Road, String> {

    /**
     * The SiedlerBoardTextView will print the field
     * The {@code toString()} methods are used for the Land, Settlement, Road
     *
     * @param board the siedlerBoard
     */
    public SiedlerBoardTextView(SiedlerBoard board) {
        super(board);
    }
}
